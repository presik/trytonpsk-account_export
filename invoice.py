#This file is part of Tryton. The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


__all__ = ['Invoice']


class Invoice:
    __metaclass__ = PoolMeta
    __name__ = 'account.invoice'
    trade_term = fields.Char('Trade Term')
    transportation_mode = fields.Char('Transportation Mode')
    carrier = fields.Many2One('carrier', 'Carrier')

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
